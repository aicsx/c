/* Scrivere un programma che richieda in ingresso tre valori interi distinti e ne determini il maggiore. */

#include <stdio.h>

int main()
{
	int a, b, c;
	printf("Digita tre valori interi per a,b,c in sucessione.\nEsempio: 1+Enter\n");
	scanf("%d %d %d", &a, &b, &c);

	printf("I valore da te inseriti sono: a=%d b=%d c=%d\n", a, b, c);
	
if((a>b) && (a>c))  	 
	printf("a è maggiore di b e di c\n"); 
else
	printf("a non è maggiore di b e c\n");
 
if((b>c) && (b>a))
	printf("b è maggiore di a e c\n");
else
	printf("b non è maggiore di a e c\n");
  
if ((c>a) && (c>b))
	printf("c è maggiore di a e b\n");
else
	printf("c non è maggiore di a e b\n");	

}

