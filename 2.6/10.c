/* Traccia: ripetere esercizio 1° utilizzando l'espressione condizionale con l'operatore "?:"
Dove per esercizio 1° rircordo per me stesso che sto sempre mbriaco era: 
Scrivere un programma che richieda in ingresso tre valori interi distinti e ne determini il maggiore.
Traccia svolta da ax[at]slackware.eu 
Tempo di svolgimento: "sarò onesto come al solito" e quindi a tal proposito questa volta cambia l'unità di misura,
26 min & 47 sec. Mi sono inceppato sull'operatore condizionale, avrei potuto ma evito per la vostra mentalità... 
evidentemente gli anni passati sono effetivamente tanti.
NOTA BENE: rispetto alla traccia 1 classica e rispetto alla prima revisione di questa traccia è stato modificata
la struttura del listato per accettare numeri "float".. come spesso suggerito dall'amico bisco: equivale ad uscire fuori traccia
per accettare numeri con la virgola (esempio: 1.878) compresi numeri interi+ (esempio: 10), e compresi numeri con numeratore -numero(esempio: -1.590).
Tradotto in fase di esame sareste considerati: molto meno che scardamente sufficiente. Nel nostro caso specifico: Chi se ne fotte! 
Se non vi ricordate cos'è una var float vi consiglio vivamente di rivederla. Vi servirà più avanti *
NOTA MOLTO BENE: l'espressione condizionale con operatore ?: dimezza il code in generale. MOOOLTO consigliato.
Non è mai troppo tardi per rimettersi a ripetere le cose. Così la vedo io. SALUTE!!!! BRINDIAMOOOOOO!!!
*/

#include <stdio.h>

float a,b,c;

int main()
{

	printf("Digita i tre valori da assegnare rispettivamente ad a,b,c.\nEsempio: numero+Enter,-numero+Enter,-numero.numero+Enter\n");
	scanf("%f %f %f", &a, &b, &c);
	printf("\nIl valore assegnato alle variabili è:\na= %f\nb= %f\nc= %f\n\n", a, b, c);

	((a>b) && (a>c) ? printf("a è il maggiore.\n\n") : printf("")); 
	((b>c) && (b>a) ? printf("b è il maggiore.\n\n") : printf(""));
	((c>a) && (c>b) ? printf("c è il maggiore.\n\n") : printf(""));
		
}
