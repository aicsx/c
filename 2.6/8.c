/* Traccia: Utilizzando l’espressione condizionale ?: scrivere un programma che, dati tre valori interi memorizzati nelle variabili a, b e c, assegna a d:
• il volume del parallelepipedo di lati a, b e c se il valore di a al quadrato sommato a b è diverso da c;
• la somma di a, b e c, altrimenti. 
Svolto da ax[at]slackware.eu "evviva i tempi andati".
Tempo di esecuzione: 120 secondi a traccia risolta. Altri 800 e passa secondi con il mio caro e fidato amico bisco sul perchè l'esercizo doveva finire li e perchè io lo volevo istruzionare per farlo printare a risoluzione. Ma che volete scusate ? che beviamo a fare sennò ? 
*/

#include <stdio.h>

int main()
{
	int a, b, c, d;
	    printf("Definiamo la lunghezza dei lati del nostro parallelepipedo; \nDigita tre numeri che corrispondono alle variabili a,b,c\n");
            scanf("%d%d%d", &a, &b, &c);	
	
	    printf("\nHai assegnato:\na=%d\nb=%d\nc=%d\n", a, b, c);

       d=((a*a+b)!=c ? a*b*c : a+b+c ); // istruzione condizionale e risoluzione traccia 
	  if( d==a*b*c ) // aggiunta di print alla traccia per sapere come è stata calcolata l'istruzione e il risultato
	    printf("il valore di d=a*b*c è: %d\n", d);
	  else  
	    printf("il valore di d=a+b+c è: %d\n", d); 

}	    

