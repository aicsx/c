/* Ripetere l’Esercizio 2 nell’ipotesi che i quattro valori possano anche essere tutti uguali, caso nel quale il messaggio da visualizzare dev’essere Valori identici. */

#include <stdio.h>

int main()
{
	int a, b, c, d;
	printf("Digita 4 valori interi da assegnare ad a,b,c,d.\nEsempio: -20(+Enter)\n");
	scanf("%d %d %d %d", &a, &b, &c, &d);

	printf("\nLe variabili assegnate sono:\na=%d\nb=%d\nc=%d\nd=%d\n\n", a, b, c, d);

if (((a>b && a>c)) && (a>d))
	printf("a è il maggiore.\n");
if (((b>c) && (b>d)) && (b>a))
	printf("b è il maggiore.\n");
if (((c>d) && (c>a)) && (c>b))
	printf("c è il maggiore.\n");
if (((d>a) && (d>b)) && (d>c))
	printf("d è il maggiore.\n");

if (((a==b) && (c==d)) && (b==c))
	printf("Valori identici.\n");
}
