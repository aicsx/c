/* Rinfrescate di c. compiti da svolgere a casa con il vino!
Traccia: Scrivi un programma che richieda in ingresso 3 valori interi distinti e che individui il maggiore e anche il minore dei tre numeri in input. 
svolto dall'ex allievo ax[at]slackware.eu sotto massiccie dosi di alcool per verificare lo stato di memoria.
Tempo totale di scrittura: 65.28 secondi ... ma il vino è decisamente molto forte. Commento scritto post scrittura a tempo azzerato :P 
*/

#include <stdio.h>

int main()
{

	int a, b, c; // definisco le 3 variabili intere
	printf("Immetti 3 numeri interi da assegnare ad a,b,c.\n");
	scanf("%d %d %d", &a, &b, &c); // prendo input e lo assegno alle var
	printf("\nLe var assegnate sono:\na=%d\nb=%d\nc=%d\n\n", a, b, c); 

	if((a>b) && (a>c)) 
		  printf("a è il maggiore\n"); 
		else if((a<b) && (a<c))
		  	printf("a è il minore\n");
	if((b>c) && (b>a))
	  	  printf("b è il maggiore\n");
  	  	else if((b<c) && (b<a))
	  	  	printf("b è il minore\n");
	if((c>a) && (c>b))
		  printf("c è il maggiore\n");
		else if((c<a) && (c<b))
			printf("c è il minore\n");
	        	   
}
