/* Sono tanti questi listati... non ho capito come mai le mie costanti sono sempre uguali: #define TOP vino, #define DIO fumo; 
Compito a casa tanto per cambiare svolto dall'allievo sempre vecchio ax[at]slackware.eu
Traccia: Supponendo che le variabili intere x, y abbiano valori 12, 45 e che le variabili carattere a e b abbiano valori “t” e “T”, le seguenti espressioni restituirebbero vero o falso?
1) (x>y) || (a!=b)
2) (y>x) && (a==b)
3) (a!=b) && !(x>y)
4) x || (y<x)
5) a == (b='t')
6) !x
Scrivere un programma che verifichi le risposte date.
*/

#include <stdio.h>

int x, y; // global var
char a, b;

int main()
{
	x=12; // int var
	y=45; // int var
	a='t'; //char var
	b='T'; // char var

	printf("Variabili intere:\n");
	printf("x=12\ny=45\n");
	printf("Variabili carattere:\n");
	printf("a='%c'\nb='%c'\n\n", a, b); // doppio \n lascio lo spazio stringa

	printf("(x>y) || (a!=b) è: ");
	if((x>y) || (a!=b))
	  printf("VERO\n");
	else printf("FALSO\n");

	printf("(y>x) && (a==b) è: ");
	if((y>x) && (a==b))
	  printf("VERO\n");
	else printf("FALSO\n");

	printf("(a!=b) && !(x>y) è: ");
	if((a!=b) && !(x>y))
	  printf("VERO\n");
	else printf("FALSO\n");

	printf("x || (y<x) è: ");
	if(x || (y<x))
	  printf("VERO\n");
	else printf("FALSO\n");

	printf("a == (b='t') è: ");
	if(a == (b='t'))
	  printf("VERO\n");
	else printf("FALSO\n");

	printf("!x è: ");
	if(!x)
	  printf("VERO\n\n");
	else printf("FALSO\n\n");

}
