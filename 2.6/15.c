/* Scrivere un programma che, richiesti in input tre numeri interi, a seconda dei casi visualizzi una delle seguenti
risposte:
Tutti uguali
Due uguali e uno diverso
Tutti diversi.

ax[at]slackware.eu
*/

#include <stdio.h>

int a, b ,c;

int main()
{

	printf("Inserisci 3 numeri interi da assegnare ad a, b, c\nEsempio: 1, 2, 3\nTocca a te:\t");
	scanf("%d, %d, %d", &a, &b, &c);

	printf("\nHai assegnato a=%d, b=%d, c=%d\n", a, b, c);

	if((a==b) && (b==c)) 
	  printf("Tutti uguali\n");
	  else if(a==b && a!=c)
	   printf("Due uguali e uno diverso\n");
	  else if(b==c && b!=a)
	    printf("Due uguali e uno diverso\n");
	  else if(c==a && c!=b)
            printf("Due uguali e uno diverso\n");
	else
 	 printf("Tutti diversi\n");
}
