/* Compiti a casa di rinfresco mnemonico .. sempre sotto stresso uso di alcool!
Compito svolto dall'allievo vecchio ax[at]slackware.eu
Traccia: Se le variabili intere a, b e c hanno rispettivamente valore 5, 35 e 7, quali valore viene assegnato alla variabile
ris dalle seguenti espressioni?

1) ris=a+b*c
2) ris=(a>b)
3) ris=(a+b) * (a<b)
4) ris=(a+b) && (a<b)
5) ris=(a+b) || (a>b)
6) ris=(a*c-b) || (a>b)
7) ris=((a*c) != b) || (a>b)
8) ris=(a>b) || (a<c) || (c==b)
Scrivere un programma che verifichi le risposte date.

Tempo totale di esecuzione: 187.49 sec. Commento scritto post scrittura compito ;P
sto decisamente mbriaco!!! sennò i 3 minuti non si spiegano!
*/

#include <stdio.h>
#include <stdlib.h> // includo la libreria per i calcoli matematici di algebra

int a, b, c; // definisco le variabili statiche globali
int ris; // definisco la variabile statica globale

int main()
{
	a=5; b=35; c=7; // assegno i valori alle var a,b,c
	printf("Variabili assegnate statiche:\na=%d \n", a);
	printf("b=%d \n", b);
	printf("c=%d \n\n", c); 
	
	ris=a+b*c;
	printf("Il valore ris=a+b*c è: %d \n", ris);
	ris=(a>b);
	printf("Il valore ris=(a>b) è: %d \n", ris);
	ris=(a+b) * (a<b);
	printf("Il valore ris=(a+b) * (a<b) è: %d \n", ris);
	ris=(a+b) && (a<b);
	printf("Il valore ris=(a+b) && (a<b) è: %d \n", ris);
	ris=(a+b) || (a>b);
	printf("Il valore ris=(a+b) || (a>b) è: %d \n", ris);
	ris=(a*c-b) || (a>b);
	printf("Il valore ris=(a*c-b) || (a>b) è: %d \n", ris);
	ris=((a*c) != b ) || (a>b);
	printf("Il valore ris=((a*c) != b ) || (a>b) è: %d \n", ris);
	ris=(a>b) || (a<c) || (c==b);
	printf("Il valore ris=(a>b) || (a<c) || (c==b) è: %d \n", ris);
	
}


