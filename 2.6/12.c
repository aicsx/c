/* Traccia: Scrivere un programma che, richiesto il numero AA rappresentante un anno, verifichi se questo è bisestile.
Suggerimento: un anno è bisestile se è divisibile per 4 ma non per 100 (cioè si escludono gli anni-secolo).
ax[at]slackware.eu
*/

#include <stdio.h>
#define A 2000 // dichiaro in global il secolo corrente

int aa; // dichiaro aa

int main()
{

	printf("Calcoliamo se un anno è bisestile.\nDigita l'anno in formato \"AA\".\nNota: per AA si intende un numero intero di due cifre che rappresenta un anno del secolo corrente.\n");
	scanf("%d", &aa); // chiedo in input il valore di aa
	int AA; // dichiaro la variabile anno finale	
	int anno; 

	AA=(aa>=0 && aa<=99 ? printf("\nAA=%d\n", anno=A+aa) : printf("\nAA=Anno non valido\n")); // condizione di verifica per la variabile anno

	if(anno) 
	  ((anno%4==0) && (anno%100!=0) ? printf("Anno bisestile\n") : printf("Anno standard\n")); 

}
