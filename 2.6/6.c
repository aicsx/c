/* Compiti a casa di rinfresco menmonico .. sempre sotto stresso uso di alcool!
Compito svolto dall'allievo vecchio ax[at]slackware.eu
Traccia: Se le variabili intere a, b e c avessero gli stessi valori di partenza dell’esercizio precedente, le seguenti espressioni restituirebbero vero o falso?

(a>b) || (c>a))
(c>a) && (a>b))
!(a>b) && (c>a))
!(a>b) || !(c>a))
(a==c) || ((a<b) && (b<c))
(a!=c) || ((a<b) && (b<c))

Scrivere un programma che verifichi le risposte date.

Tempo totale di esecuzione: 351.70 sec. Commento scritto post scrittura compito ;P
sto decisamente sempre mbriaco! assai proprio...
*/

#include <stdio.h>

int a, b, c; // definisco le variabili statiche globali
int ris; // definisco la variabile statica globale

int main()
{
	a=5; b=35; c=7; // assegno i valori alle var a,b,c
	printf("\nVariabili assegnate statiche:\na=%d \n", a);
	printf("b=%d \n", b);
	printf("c=%d \n\n", c); 

	 if(ris=(a>b) || (c>a))
	   printf("ris=(a>b) || (c>a)) è: VERO\n");
	  else printf("ris=(a>b) || (c>a)) è: FALSO\n");

	 if(ris=(c>a) && (a>b))
	   printf("ris=(c>a) && (a>b) è: VERO\n");
	  else printf("ris=(c>a) && (a>b) è: FALSO\n");

	 if(ris=!(a>b) && (c>a))
	   printf("ris=!(a>b) && (c>a) è: VERO\n");
	  else printf("ris=!(a>b) && (c>a) è: Falso\n");

	 if(ris=!(a>b) || !(c>a)) 
	   printf("ris=!(a>b) || !(c>a) è: VERO\n");
	  else printf("ris=!(a>b) || !(c>a) è: FALSO\n");

	 if(ris=(a==c) || ((a<b) && (b<c))) 
	   printf("ris=(a==c) || ((a<b) && (b<c)) è: VERO\n");
	  else printf("ris=(a==c) || ((a<b) && (b<c)) è: FALSO\n");

	 if(ris=(a!=c) || ((a<b) && (b<c)))
	   printf("ris=(a!=c) || ((a<b) && (b<c)) è: VERO\n\n");
	  else printf("ris=(a!=c) || ((a<b) && (b<c)) è: FALSO\n\n");
	
}


