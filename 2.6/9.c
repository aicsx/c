/* Traccia: Scrivere un programma che visualizzi il seguente menu:
MENU DI PROVA
a) Per immettere dati
b) Per determinare il maggiore
c) Per determinare il minore
d) Per ordinare
e) Per visualizzare
	Scelta: _

quindi attenda l’immissione di un carattere da parte dell’utente e visualizzi una scritta corrispondente alla scelta effettuata, del tipo: “In esecuzione l'opzione a”. Se la scelta non è tra quelle proposte (a, b, c, d, e) deve essere visualizzata la scritta: “Opzione inesistente”. Si utilizzi il costrutto switch-case e la funzione getchar.
BEVUTO: ax[at]slackware.eu
tempo di svolgimento: 240 secondi, il vino è sempre troppo di sera.
*/

#include <stdio.h>

int menu;

int main ()
{

	char a, b, c, d, e;
	printf("-- MENU DI PROVA\n");
	printf("a. Per immettere dati\n");
	printf("b. Per determinare il maggiore\n");
	printf("c. Per determinare il minore\n"); 
	printf("d. Per ordinare\n");
	printf("e. Per visualizzare\n");
	menu = getchar();
 
switch(menu) {
	case 'a':
	     printf("In esecuzione opzione a\n");
	     break;
	case 'b':
	     printf("In esecuzone opzione b\n");
	     break;
	case 'c':
	     printf("In esecuzione opzione c\n");
	     break;
	case 'd':
	     printf("In esecuzone opzione d\n");
	     break;
	case 'e':
	     printf("In esecuzione opzione e\n");
	     break;
	default:
	     printf("Opzione inesistente\n"); 
	}
}
