/* Codificare un programma che calcoli la seguente espressione: y=xa+b, dove x è uguale a 5, a è uguale a 18 e b è
uguale a 7; x, a, e b devono essere dichiarate come variabili intere. Si visualizzi infine il valore finale:
y = 97 */

/* start */
#include <stdio.h>

int main()
{
	int x, a, b;
	x = 5;
	a = 18;
	b = 7;
	
	float y;
	y = (x*a)+b;
	
	printf("y = %.f\n", y); 

} 
