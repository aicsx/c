/* Scrivere un programma che calcoli e visualizzi le seguenti espressioni:
a = ZERO - abs(x)
b = TOP - abs(y)
c = a*b
dove x e y sono variabili intere immesse dall’utente, ZERO e TOP sono costanti intere di valore 0 e 1000. */

#include <stdio.h>
#include <stdlib.h>	/* libreria per (math.h) abs */
#define ZERO 0		/* costante */
#define TOP 1000 	/* costante */

int main ()
{
	int x, y, a, b, c;
	
	x = abs(300);     // x is assigned to 300
   	y = abs(-50);    // y is assigned to -50
	
	printf("il valore abs(x) è %d\n", x);
	printf("il valore abs(y) è %d\n", y);

	a = ZERO - abs(x);
	b = TOP - abs(y);
	
	printf("a = %d\n", a);
	printf("b= %d\n", b);

	c = a*b;

	printf("il valore di c è: %d\n", c);
}
