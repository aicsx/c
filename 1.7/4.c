/* Modificare il programma dell’esercizio precedente in modo che utilizzi le costanti A e B invece delle variabili a e b.*/

/* start */
#include <stdio.h>
#define A 18
#define B 7

int main()
{
	int a, b, x;
        printf("\nL'espressione è y=xa+b, con risultato y=97\n\na=18\nb=7\nx=5\nProva a digitare tu il valore di x: ");
	scanf("%d", &x);
	
	float y;
	y = (x*A)+B;
	
	printf("y = %.f\n\n", y); 

} 
