/* Trasformare il programma dell’esercizio precedente in modo che il valore di x venga richiesto all’utente in fase di
esecuzione.*/

/* start */
#include <stdio.h>

int main()
{
	int a, b, x;
	a = 18;
	b = 7;
        printf("\nL'espressione è y=xa+b, con risultato y=97\n\na=18\nb=7\nx=5\nProva a digitare tu il valore di x: ");
	scanf("%d", &x);
	
	float y;
	y = (x*a)+b;
	
	printf("y = %.f\n\n", y); 

} 
